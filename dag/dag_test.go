package dag

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strings"
	"testing"
)

func TestName(t *testing.T) {
	str := "{$s}123"
	println(strings.Replace(str, "{$s}", "%s", -1))
}

func TestNam1e(t *testing.T) {
	rge, err := regexp.Compile(`^[0-9]*$`)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(rge.Match([]byte("123")))
}

func TestName2(t *testing.T) {
	yl, err := ioutil.ReadFile(filepath.Join("../../dag-cli/giteego.yaml"))
	if err != nil {
		fmt.Println(err)
		return
	}
	dag, err := ioutil.ReadFile(filepath.Join("../../dag-cli/dag.yaml"))
	if err != nil {
		fmt.Println(err)
		return
	}
	err = CheckFileWithDag(yl, dag)
	if err != nil {
		fmt.Println(err)
		return
	}
}
