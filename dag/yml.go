package dag

import "time"

type Pipeline struct {
	Name        string              `yaml:"name,omitempty" json:"name"`
	Version     string              `yaml:"version,omitempty" json:"version"`
	DisplayName string              `yaml:"displayName,omitempty" json:"displayName"`
	Triggers    map[string]*Trigger `yaml:"triggers,omitempty" json:"triggers"`
	Variables   map[string]string   `yaml:"variables,omitempty" json:"variables"`
	Stages      []*Stage            `yaml:"stages,omitempty" json:"stages"`
}

type Trigger struct {
	AutoCancel     bool       `yaml:"autoCancel,omitempty" json:"autoCancel,omitempty"`
	Timeout        string     `yaml:"timeout,omitempty" json:"timeout,omitempty"`
	Branches       *Condition `yaml:"branches,omitempty" json:"branches,omitempty"`
	Tags           *Condition `yaml:"tags,omitempty" json:"tags,omitempty"`
	Paths          *Condition `yaml:"paths,omitempty" json:"paths,omitempty"`
	Notes          *Condition `yaml:"notes,omitempty" json:"notes,omitempty"`
	CommitMessages *Condition `yaml:"commitMessages,omitempty" json:"commitMessages,omitempty"`
}

type Condition struct {
	Include []string `yaml:"include,omitempty" json:"include,omitempty"`
	Exclude []string `yaml:"exclude,omitempty" json:"exclude,omitempty"`
}

type Stage struct {
	Stage             string            `yaml:"stage" json:"stage"`
	PipelineVersionId string            `json:"pipelineVersionId"   yaml:"-"`
	BuildId           string            `json:"buildId"   yaml:"-"`
	Name              string            `yaml:"name,omitempty" json:"name"`
	DisplayName       string            `yaml:"displayName,omitempty" json:"displayName"`
	Status            string            `json:"status"   yaml:"-"`
	Error             string            `json:"error"   yaml:"-"`
	ExitCode          int               `json:"exit_code"   yaml:"-"`
	Started           time.Time         `json:"started"   yaml:"-"`
	Stopped           time.Time         `json:"stopped"   yaml:"-"`
	Finished          time.Time         `json:"finished"   yaml:"-"`
	Created           time.Time         `json:"created"   yaml:"-"`
	Updated           time.Time         `json:"updated"   yaml:"-"`
	Version           string            `json:"version"   yaml:"-"`
	OnSuccess         bool              `json:"on_success"   yaml:"-"`
	OnFailure         bool              `json:"on_failure"   yaml:"-"`
	Labels            map[string]string `json:"labels,omitempty" yaml:"-"`
	Jobs              []*Job            `yaml:"steps,omitempty" json:"steps"`
}

type Job struct {
	Job             string            `yaml:"step" json:"step"`
	DisplayName     string            `yaml:"displayName,omitempty" json:"displayName"`
	Name            string            `yaml:"name,omitempty" json:"name"`
	Environments    map[string]string `yaml:"environments,omitempty" json:"environments"`
	Commands        []interface{}     `yaml:"commands,omitempty" json:"commands"`
	Command         string            `yaml:"-" json:"-"`
	Number          int               `json:"number"  yaml:"-"`
	Status          string            `json:"status"  yaml:"-"`
	Created         time.Time         `json:"created"   yaml:"-"`
	Error           string            `json:"error,omitempty"  yaml:"-"`
	ErrIgnore       bool              `json:"errignore,omitempty"  yaml:"-"`
	ExitCode        int               `json:"exitCode"  yaml:"-"`
	Started         time.Time         `json:"started,omitempty"  yaml:"-"`
	Stopped         time.Time         `json:"stopped,omitempty"  yaml:"-"`
	Finished        time.Time         `json:"finished"  yaml:"-"`
	Version         string            `json:"version"  yaml:"-"`
	DependsOn       []string          `yaml:"dependsOn,omitempty" json:"dependsOn"`
	Image           string            `yaml:"image,omitempty" json:"image"`
	Artifacts       []*Artifact       `yaml:"artifacts,omitempty" json:"artifacts"`
	DependArtifacts []*DependArtifact `yaml:"dependArtifacts,omitempty" json:"dependArtifacts"`
	Plugined        bool              `yaml:"-" json:"plugined"`
}

type Artifact struct {
	BuildName string `json:"buildName"  yaml:"-"`
	StageName string `json:"stageName"  yaml:"-"`
	JobName   string `json:"stepName"  yaml:"-"`

	Name  string `yaml:"name,omitempty" json:"name"`
	Scope string `yaml:"scope,omitempty" json:"scope"`
	Path  string `yaml:"path,omitempty" json:"path"`

	Repository string `yaml:"repository,omitempty" json:"repository"`

	Value string `yaml:"value,omitempty" json:"value"`
}

type DependArtifact struct {
	BuildName string `yaml:"buildName,omitempty" json:"buildName"`
	StageName string `yaml:"stageName,omitempty" json:"stageName"`
	JobName   string `yaml:"jobName,omitempty" json:"stepName"`

	Type       string `yaml:"type,omitempty" json:"type"`
	Repository string `yaml:"repository,omitempty" json:"repository"`
	Name       string `yaml:"name,omitempty" json:"name"`
	Target     string `yaml:"target,omitempty" json:"target"`
	IsForce    bool   `yaml:"isForce,omitempty" json:"isForce"`

	SourceStage string `yaml:"sourceStage,omitempty" json:"sourceStage"`
	SourceJob   string `yaml:"sourceStep,omitempty" json:"sourceStep"`

	Value string `json:"value"  yaml:"-"`
}
