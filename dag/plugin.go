package dag

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v2"
	"reflect"
)

type ShellDetail struct {
	Type        string      ` yaml:"type" json:"type,omitempty"`
	Rule        interface{} `yaml:"rule"`
	DisplayName string      `yaml:"displayName"`
	Description string      `yaml:"description"`
	Required    bool        `yaml:"required" `
}

type Parameter struct {
	Type        string      ` yaml:"type" json:"type,omitempty"`
	Rule        interface{} ` yaml:"rule" json:"rule,omitempty"`
	Description string      ` yaml:"description" json:"description,omitempty"`
	DisplayName string      ` yaml:"displayName" json:"displayName,omitempty"`
	Required    bool        ` yaml:"required" json:"required,omitempty"`
}

func CheckPlugin(mp map[string]string, pl []byte) error {
	ps := map[string]*Parameter{}
	err := yaml.Unmarshal(pl, &ps)
	if err != nil {
		return err
	}
	err = checkBasePlugin(ps)
	if err != nil {
		return err
	}
	return checkPlugin(mp, ps)
}

func checkPlugin(mp map[string]string, pl map[string]*Parameter) error {
	if len(pl) <= 0 {
		return nil
	}
	if len(mp) <= 0 {
		return fmt.Errorf("yml为空")
	}
	for k, v := range pl {
		s, ok := mp[k]
		if v.Required && (!ok || s == "") {
			return fmt.Errorf("%s属性不能为空", k)
		}
		if !ok || s == "" {
			continue
		}
		err := checkByType(v, s)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkByType(pt *Parameter, s string) error {
	if pt.Type == "shell" {
		return nil
	}
	if pt.Rule == nil {
		return nil
	}
	rules, err := getRules(pt.Type, pt.Rule)
	if err != nil {
		return err
	}
	if !dagEnum(rules, s) {
		return fmt.Errorf("%s不符合规则", s)
	}
	return nil
}

func checkBasePlugin(pl map[string]*Parameter) error {
	shells := 0
	for _, p := range pl {
		if p.Type == "shell" {
			shells++
		}
		if shells > 1 {
			return errors.New("插件只允许使用一个shell插件")
		}
	}
	return nil
}

func getRules(ty string, rs interface{}) ([]string, error) {
	switch rs.(type) {
	case string:
		return []string{rs.(string)}, nil
	case []interface{}:
		var ls []string
		for _, v := range rs.([]interface{}) {
			ls = append(ls, fmt.Sprintf("%v", v))
		}
		return ls, nil
	case []string:
		return rs.([]string), nil
	default:
		return nil, fmt.Errorf("未处理的rules类型%v", reflect.TypeOf(rs))
	}
}
