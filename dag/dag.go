package dag

import (
	"encoding/json"
	"fmt"
	"gopkg.in/yaml.v2"
	"reflect"
	"regexp"
	"strings"
)

const dagTemplate = `
version:
  type: enum   # string,enum,mixed
  rule: ["1.0"]
  displayName: 
  description:

triggers:
  type: enum
  rule: [ 'push','pr' ]
  displayName: 
  description:

name:
  type: string
  rule:
    name: '[a-zA-Z]'
  displayName: 流水线名称
  description: 流水线名称
  message: # 错误提示的文本设定
    name: "{$s}必须为英文"

displayName:
  type: string
  rule:
    length: '^\d{1,10}$'
  displayName: 流水线名称
  description: 流水线名称的描述
  required: true
  message: # 错误提示的文本设定
    name: "{$s}必须为英文"

steps:
  type: list
  rule:
    mix: 0
    max: 100
  displayName: 配置描述版本号
  description: 配置描述文件版本号,目前只有 1.0
  required: true
  message: # 错误提示的文本设定
    name: "{$s}数量超过限制"

stages:
  type: list
  rule:
    mix: 0
    max: 100
  displayName: 配置描述版本号
  description: 配置描述文件版本号,目前只有 1.0
  required: true
  message: # 错误提示的文本设定
    name: "{$s}数量超过限制"
`
const (
	Version     = "版本(version)"
	Triggers    = "触发器(triggers)"
	Name        = "流水线名称(name)"
	DisplayName = "描述(displayName)"
	Steps       = "steps"
	Stages      = "stages"
	Min         = "min"
	Max         = "max"
)

var (
	ErrMsg           = "%s 不匹配"
	ErrRequired      = "%s 为必填"
	ErrDataStructure = "%s 格式不正确"
	ErrDAG           = "%s DAG格式不正确"
	ErrRulesEmpty    = "%s DAG的Rules为空"
	ErrRules         = "%s Rules错误 %v"
)

type Detail struct {
	Kind        string            `yaml:"type"`
	Rule        interface{}       `yaml:"rule"`
	DisplayName string            `yaml:"displayName"`
	Description string            `yaml:"description"`
	Required    bool              `yaml:"required" `
	Message     map[string]string `yaml:"message"`
}

func CheckJson(yl []byte) error {
	pipeline := &Pipeline{}
	err := json.Unmarshal(yl, pipeline)
	if err != nil {
		return err
	}
	dags := make(map[string]*Detail, 0)
	err = yaml.Unmarshal([]byte(dagTemplate), dags)
	if err != nil {
		return err
	}
	return check(pipeline, dags)
}

func CheckJsonWithDag(yl []byte, dg []byte) error {
	pipeline := &Pipeline{}
	err := json.Unmarshal(yl, pipeline)
	if err != nil {
		return err
	}
	dags := make(map[string]*Detail, 0)
	err = yaml.Unmarshal(dg, dags)
	if err != nil {
		return err
	}
	return check(pipeline, dags)
}

func CheckFile(yl []byte) error {
	pipeline := &Pipeline{}
	err := yaml.Unmarshal(yl, pipeline)
	if err != nil {
		return err
	}
	dags := make(map[string]*Detail, 0)
	err = yaml.Unmarshal([]byte(dagTemplate), dags)
	if err != nil {
		return err
	}
	return check(pipeline, dags)
}

func CheckFileWithDag(yl []byte, dg []byte) error {
	pipeline := &Pipeline{}
	err := yaml.Unmarshal(yl, pipeline)
	if err != nil {
		return err
	}
	dags := make(map[string]*Detail, 0)
	err = yaml.Unmarshal(dg, dags)
	if err != nil {
		return err
	}
	return check(pipeline, dags)
}

func check(pie *Pipeline, details map[string]*Detail) error {
	if pie == nil {
		return fmt.Errorf("pipeline is empty")
	}
	if details == nil || len(details) <= 0 {
		return fmt.Errorf("dag is empty")
	}
	err := version(pie, details)
	if err != nil {
		return err
	}
	err = pipelineName(pie, details)
	if err != nil {
		return err
	}
	err = triggers(pie, details)
	if err != nil {
		return err
	}
	err = pipelineDisplayName(pie, details)
	if err != nil {
		return err
	}
	err = pipelineStages(pie, details)
	if err != nil {
		return err
	}
	return nil
}

func pipelineName(pie *Pipeline, details map[string]*Detail) error {
	if pie.Name == "" {
		return fmt.Errorf(ErrRequired, Name)
	}
	return fieldRegexp(Name, pie.Name, details)
}
func pipelineDisplayName(pie *Pipeline, details map[string]*Detail) error {
	if pie.DisplayName == "" {
		return fmt.Errorf(ErrRequired, DisplayName)
	}
	return fieldRegexp(DisplayName, pie.DisplayName, details)
}
func pipelineStages(pie *Pipeline, details map[string]*Detail) error {
	if pie.DisplayName == "" {
		return fmt.Errorf(ErrRequired, DisplayName)
	}
	return fieldList(Stages, len(pie.Stages), details)
}

func version(pie *Pipeline, details map[string]*Detail) error {
	if detail, ok := details[Version]; ok {
		if detail == nil {
			return fmt.Errorf(ErrDAG, Version)
		}
		if pie.Version == "" && detail.Required {
			return fmt.Errorf(ErrRequired, Version)
		}
		rule := detail.Rule
		if rule == nil {
			return fmt.Errorf(ErrRulesEmpty, Version)
		}
		if reflect.TypeOf(detail.Rule).Kind() != reflect.Slice {
			return fmt.Errorf(ErrDAG, Version)
		}
		rules := detail.Rule.([]interface{})
		for _, r := range rules {
			if pie.Version == fmt.Sprintf("%v", r) {
				return nil
			}
		}
		return fmt.Errorf(ErrMsg, Version)
	}
	return nil
}
func triggers(pie *Pipeline, details map[string]*Detail) error {
	if detail, ok := details[Triggers]; ok {
		if detail == nil {
			return fmt.Errorf(ErrDAG, Version)
		}
		if pie.Triggers == nil || len(pie.Triggers) <= 0 {
			return fmt.Errorf(ErrRequired, Triggers)
		}
		rule := detail.Rule
		if rule == nil {
			return fmt.Errorf(ErrRulesEmpty, Triggers)
		}
		if reflect.TypeOf(detail.Rule).Kind() != reflect.Slice {
			fmt.Println("rule is not slice")
			return fmt.Errorf(ErrDAG, Triggers)
		}
		rules := detail.Rule.([]interface{})
		var rs []string
		for _, r := range rules {
			rs = append(rs, fmt.Sprintf("%v", r))
		}
		for k, t := range pie.Triggers {
			if !dagEnum(rs, k) {
				return fmt.Errorf(ErrMsg, Triggers)
			}
			if !t.check() {
				return fmt.Errorf(ErrDataStructure, Triggers)
			}

		}
	}
	return nil
}
func fieldRegexp(field, v string, details map[string]*Detail) error {
	if detail, ok := details[field]; ok {
		if detail == nil {
			return fmt.Errorf(ErrDAG, Version)
		}
		rule := detail.Rule
		if rule == nil {
			return fmt.Errorf(ErrRulesEmpty, field)
		}
		regs, err := getRegexp(detail)
		if err != nil || len(regs) < 0 {
			return fmt.Errorf(ErrDAG, field)
		}
		for k, regx := range regs {
			b, err := dagRegexp(regx, v)
			if err != nil {
				return fmt.Errorf(ErrRules, field, err)
			}
			if !b {
				return returnMessage(detail, k, field)
			}
		}
	}
	return nil

}
func fieldList(field string, ls int, details map[string]*Detail) error {
	if detail, ok := details[field]; ok {
		if detail == nil {
			return fmt.Errorf(ErrDAG, Version)
		}
		rule := detail.Rule
		if rule == nil {
			return fmt.Errorf(ErrRulesEmpty, field)
		}
		m, err := getList(detail)
		if err != nil {
			return fmt.Errorf(ErrDAG, field)
		}
		if !dagList(m, ls) {
			return fmt.Errorf(ErrMsg, field)
		}
	}
	return nil
}

func (c *Trigger) check() bool {
	if c.Paths != nil && (len(c.Paths.Exclude) > 0 || len(c.Paths.Include) > 0) {
		return true
	}
	if c.Tags != nil && (len(c.Tags.Exclude) > 0 || len(c.Tags.Include) > 0) {
		return true
	}
	if c.Notes != nil && (len(c.Notes.Exclude) > 0 || len(c.Notes.Include) > 0) {
		return true
	}
	if c.CommitMessages != nil && (len(c.CommitMessages.Exclude) > 0 || len(c.CommitMessages.Include) > 0) {
		return true
	}
	if c.Branches != nil && (len(c.Branches.Exclude) > 0 || len(c.Branches.Include) > 0) {
		return true
	}
	return false
}
func dagRegexp(regx, v string) (bool, error) {
	reg, err := regexp.Compile(regx)
	if err != nil {
		return false, err
	}
	return reg.Match([]byte(v)), nil
}
func dagList(m map[string]int, l int) bool {
	min := 0
	max := 0
	if mi, ok := m[Min]; ok {
		min = mi
	}
	if ma, ok := m[Max]; ok {
		max = ma
	}
	if min == 0 && max == 0 {
		return true
	}
	if min != 0 && max != 0 {
		return l >= min && l <= max
	}
	if min > 0 && max == 0 {
		return l >= min
	}
	if min == 0 && max > 0 {
		return l <= max
	}
	return false
}

func getList(d *Detail) (map[string]int, error) {
	switch d.Rule.(type) {
	case map[string]int:
		rule := d.Rule.(map[string]int)
		return rule, nil
	case map[interface{}]interface{}:
		rs := d.Rule.(map[interface{}]interface{})
		rule := make(map[string]int)
		for k, v := range rs {
			if reflect.TypeOf(v).Kind() == reflect.Int {
				rule[fmt.Sprintf("%v", k)] = v.(int)
			}
		}
		return rule, nil
	default:
		return nil, fmt.Errorf("没有匹配的规则")
	}
}

func getRegexp(d *Detail) (map[string]string, error) {
	var rs = make(map[string]string)
	switch d.Rule.(type) {
	case map[string]string:
		rule := d.Rule.(map[string]string)
		for k, r := range rule {
			rs[k] = r
		}
	case map[interface{}]interface{}:
		rule := d.Rule.(map[interface{}]interface{})
		for k, r := range rule {
			rs[fmt.Sprintf("%v", k)] = fmt.Sprintf("%v", r)
		}
	default:
		return nil, fmt.Errorf("没有匹配的规则")
	}
	return rs, nil
}

func dagEnum(enums []string, v string) bool {
	for _, e := range enums {
		if e == v {
			return true
		}
	}
	return false
}

func returnMessage(d *Detail, messageKey, name string) error {
	err := fmt.Errorf(ErrMsg, name)
	if d.Message == nil || len(d.Message) <= 0 {
		return err
	}
	if msg, ok := d.Message[messageKey]; ok {
		err = fmt.Errorf(strings.Replace(msg, "{$s}", "%s", -1), name)
	}
	return err
}
