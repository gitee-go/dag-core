# dag的实现

## 简述

## 代码结构

```
.
├── README.en.md
├── README.md
├── dag             
│   ├── dag.go      dag的结构体和效验逻辑
│   ├── dag_test.go
│   ├── plugin.go   插件的结构体和效验逻辑
│   └── yml.go      yml的结构体
├── go.mod
└── go.sum
```

dag yaml文件样例
```yaml
version:
  type: enum              # 分别有 string,enum,mixed类型
  rule: ["1.0"]           # 可以是数组,正则等,根据不同类型
  displayName: 
  description:

triggers:
  type: enum
  rule: [ 'push','pr' ]
  displayName: 
  description:

name:
  type: string          
  rule:
    name: '[a-zA-Z]'       
  displayName: 流水线名称
  description: 流水线名称
  message: # 错误提示的文本设定
    name: "{$s}必须为英文"         # 规则效验不通过的文字提示

displayName:
  type: string
  rule:
    length: '^\d{1,10}$'
  displayName: 流水线名称
  description: 流水线名称的描述
  required: true
  message: # 错误提示的文本设定
    name: "{$s}必须为英文"

steps:
  type: list
  rule:
    mix: 0
    max: 100
  displayName: 配置描述版本号
  description: 配置描述文件版本号,目前只有 1.0
  required: true
  message: # 错误提示的文本设定
    name: "{$s}数量超过限制"

stages:
  type: list
  rule:
    mix: 0
    max: 100
  displayName: 配置描述版本号
  description: 配置描述文件版本号,目前只有 1.0
  required: true
  message: # 错误提示的文本设定
    name: "{$s}数量超过限制"

```